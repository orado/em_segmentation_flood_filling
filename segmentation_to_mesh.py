from pathlib import Path
import argparse

import numpy as np
import tifffile as tiff

import neuroglancer

parser = argparse.ArgumentParser(description='create meshes')
parser.add_argument('--img_path_str', type=str, help='path to images')
parser.add_argument('--dst_path_str', type=str, help='path to destination mesh folder')
parser.add_argument('--voxel_size', nargs=3, type=int, help='voxel size (x, y, z)')
parser.add_argument('--max_voxels_for_mesh', type=int, help='maximum number of voxeld in a segment to compute mesh')
parser.add_argument('--max_quadrics_error', type=int, help='default neuroglancer is 1e6, bigger - more simple mesh')

args = parser.parse_args()

img_path_str = args.img_path_str
dst_path_str = args.dst_path_str
voxel_size = args.voxel_size
max_voxels_for_mesh = args.max_voxels_for_mesh
max_quadrics_error = args.max_quadrics_error

print(  "\r\nimg_path_str =         ",img_path_str                 ,
        "\r\ndst_path_str =         ",dst_path_str                 ,
        "\r\nvoxel_size =           ",voxel_size                   ,
        "\r\nmax_voxels_for_mesh =  ",max_voxels_for_mesh          ,
        "\r\nmax_quadrics_error =   ",max_quadrics_error           ,
        "\r\n")

json_descriptor = '{{"fragments": ["mesh.{}.{}"]}}'

img_path = Path(img_path_str)
dst_path = Path(dst_path_str)

print("Loading tiffs")
mesh_list = []
for f in sorted(img_path.glob('*.tif')):

    A = tiff.imread(str(f))
    mesh_list.append(A)
    
print("Getting images to np.stack")
mesh = np.dstack(mesh_list)

print("Computing unique IDs")
ids, counts = np.unique(mesh[:], return_counts = True)

print("Converting IDs from np.uint16 to int")
ids = [ int(id) for id in ids]

print("Unique: [ ID, count ]")
print(ids, counts)

if max_voxels_for_mesh != -1:
    print("max_voxels_for_mesh != -1")

    #mesh = np.transpose(mesh, (2, 1, 0))
    transpose_shape = (1, 0, 2)
    print("Transposing", transpose_shape)
    mesh = np.transpose(mesh, transpose_shape) # seems like images loaded yxz, convert to xyz


    print("Creating neuroglancer.LocalVolume")
    vol=neuroglancer.LocalVolume(
                mesh,
                dimensions=neuroglancer.CoordinateSpace(
                    scales=voxel_size
                    , units = ['m', 'm', 'm']# units=['m', 'm', 'm'], # localhost neuroglancer uses meters...
                    , names=['x', 'y', 'z']),
                voxel_offset=[0, 0, 0],
                mesh_options=dict(max_quadrics_error=max_quadrics_error) # max_quadrics_error=0
    )
    
    dst_path.joinpath('mesh').mkdir(exist_ok=True, parents=True)
    
    # mesh folder should be in a root folder with segm,entation where info about scales located
    # also, mesh folder should have its own info file with text 
    #
    # {"@type":"neuroglancer_legacy_mesh"}
    #
    # write info file
    with open(
        str(dst_path / 'mesh' / ''.join(('info'))), 'w') as infofile:
            infofile.write('{"@type":"neuroglancer_legacy_mesh"}')
    
            print('infofile.write')

    for ID, voxel_count in zip(ids[1:], counts[1:]):
        
        if voxel_count > max_voxels_for_mesh:
            print("Skipping: ID, voxels, max_voxels", ID, voxel_count, max_voxels_for_mesh)
            continue
            
        print('Getting mesh data for segment [ID, voxels]', ID, voxel_count)
        mesh_data = vol.get_object_mesh(ID)
        print('got mesh data, saving...')
        with open(
                str(dst_path / 'mesh' / '.'.join(
                    ('mesh', str(ID), str(ID)))), 'wb') as meshfile:
            meshfile.write(mesh_data)
        print('mesh saved')
        with open(
                str(dst_path / 'mesh' / ''.join(
                    (str(ID), '_0'))), 'w') as ff:
            ff.write(json_descriptor.format(ID, ID))
        print('mesh json saved')
    
    print('done')
