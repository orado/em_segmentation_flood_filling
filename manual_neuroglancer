-- Links --

https://github.com/google/neuroglancer

https://github.com/HumanBrainProject/neuroglancer-scripts

https://github.com/HumanBrainProject/neuroglancer-docker

https://neurodata.io/help/precomputed/

https://readthedocs.org/projects/neuroglancer-scripts/downloads/pdf/latest/

https://github.com/google/neuroglancer/blob/master/src/neuroglancer/datasource/precomputed/meshes.md#legacy-single-resolution-mesh-format

https://github.com/google/neuroglancer/blob/master/src/neuroglancer/datasource/precomputed/volume.md

https://groups.google.com/g/neuroglancer



Below the how-to for viewing locally stored tif data with Neuroglancer:
 - installing Neuroglancer viewer running in a Docker container
 - creating "precomputed" data format from  EM images and segmentation *.tif stacks
 - generating mesh from segmentation labels *.tif
 - starting viewer and loading the data

Setup has been tested with Ubuntu 18.04, 19.04 and 20.04


--- Installing Neuroglanser ---

Several of first steps are copied from official guide, section "Building"

    https://github.com/google/neuroglancer



0. Open new terminal window [you at the /home directory now]

    [you at the /home directory now]

1. First install NVM (node version manager) per the instructions here:

    https://github.com/creationix/nvm

2. Install a recent version of Node.js if you haven't already done so:

    nvm install stable

3. Clone the Neuroglancer repo 

    git clone https://github.com/google/neuroglancer.git

4. go to neuroglancer folder

    cd  neuroglancer

    [you at the /home/neuroglancer directory now]

5. Install the dependencies required by this project[neuroglancer]:

    npm i

6. Install python neuroglancer module
   (original instructions are here: https://github.com/google/neuroglancer/tree/master/python)

    cd python

    pip install -e .

    [you at the /home/neuroglancer/python directory now]

7. Test if you have properly installed python module, no errors means success

    python

    import neuroglancer

    exit()

8. Install Docker

   TODO

9. Build the neuroglancer webpack
   (original guide here: https://github.com/HumanBrainProject/neuroglancer-docker )
    
    [go back to /home/neuroglancer]

    cd ../

    npm run build

    (... it will take a while to finish ...)

10. Make sure webpack built successfully, 
    
    there should apper folders /dist/dev inside the folder /neuroglancer
    
11. Copy webpack to another folder and build docker image named 'neuroglancer'
    
    11.0 Open new terminal window and create folder 'precomputed' in a home directory, so you have /home/<user>/precomputed 
        in this folder we will put neuroglancer "precomputed" format data and provide access via http 
        (yes, access to a local folder from web browser via http protocol)

        mkdir precomputed
        
        11.0.1 create text file named "testfile" and put there some arbitrary text like "testing"
             
            cd precomputed

            cat > testfile

            [type:] testing

            [click] Ctrl+C
            
            with file explorer check that file created

    11.1 open file explorer and create a folder named "neuroglancer_docker"
    11.2 copy the /dist/dev folder from folder /neuroglancer to to folder /neuroglancer_docker, so you have folder /neuroglancer_docker/dev/
    11.3 in the /neuroglancer_docker folder create a text file named 
          
        Dockerfile

    11.4 open the Dockerfile with text editor and copy there text Below

        FROM nginx:stable

        COPY dev /neuroglancer
        COPY nginx.conf /etc/nginx/nginx.conf 
    
    11.5 Download the "nginx.conf" file from here

        https://github.com/HumanBrainProject/neuroglancer-docker

    11.6 Copy nginx.conf file to folder /neuroglancer_docker
    
    11.7 Open new terminal window and cd to /neuroglancer_docker

    11.8 Run the command to create the docker container named "neuroglancer"

        sudo docker build -t neuroglancer .

    11.9 Run the container at 8085 port and test if viewer appear, 
    
        11.9.1 Open new terminal window

            cd neuroglancer

            sudo docker run --name neuroglancer -v /home/<YOUR USERNAME>/precomputed:/precomputed:ro -v "$PWD"/dist/dev:/neuroglancer:ro -p 8085:80 neuroglancer

        11.9.2 Open web broser and go to address

            http://localhost:8085/

        - if viewer appear, proceed to next step
        
        - if 404 not found, run two command below to stop the docker 
         
            sudo docker stop $(sudo docker ps -a -q)

            sudo docker rm $(sudo docker ps -a -q)

            and next make sure you in the /neuroglancer folder when calling docker run command,
            docker need to access original /neuroglancer/dist/dev folder with the webpack you have built earlier

        11.9.3 Test the access to /precomputed folder by attempting to downnload the testfile you created earlier, go to address

            http://localhost:8085/precomputed/testfile

        - if browser starting to download file - OK, proceed to next step

        - if 404, make sure that the filenames and folders are correspond to docker start command

    12. Stop docker

        sudo docker stop $(sudo docker ps -a -q)

        sudo docker rm $(sudo docker ps -a -q)


#...That was the hardest part of the setup, if you did it, you almost done!

--- Creating the precomputed format ---
    
    0. If you have tiff stack, save it as image sequence
        ImageJ -> Save as -> Image Sequence
        
        in case if you need to transfer data between computers, between virtual machine and host or to upload to cloud,
        to save your time first zip your folder with images, transfer the zip archive and then unzip.
    
    1. Install neuroglancer-scripts, setup applies for Wondows as well
       (official docs: https://github.com/HumanBrainProject/neuroglancer-scripts)

       pip install neuroglancer-scripts

       git clone https://github.com/HumanBrainProject/neuroglancer-scripts.git

       cd neuroglancer-scripts

       pip install -e .[dev]

    2. Create the text file in a default home directory and give it descriptive name of your dataset,
       for example info_em_images.json

       so now you have /home/<user>/info_em_images.json

    3. Open the info_em_images.json with text editor and put there text below
       
       {
           "type": "image",
           "data_type": "uint8",
           "num_channels": 1,
           "scales": [
               {
                  "size": [1542, 1502, 1186],
                  "resolution": [10, 10, 5],
                  "voxel_offset": [0, 0, 0]
               } 
           ]
        }

    4. Change the size and resolution

        4.1 "size" is the [X, Y, Z] dimensions of your dataset in pixels, where
            X is the Width, 
            Y is the Height,
            Z is the number of images in a stack
        
            in example above, stack is 1542 pixels in width, 1502 pixels in height and there are 1186 frames
        
        4.2 "resolution" is the [X, Y, Z] dimensions of a single voxel(3d pixel) in nanometers,
            in example above, voxel size is 10nm width (wide), 10nm height (tall) and 5nm in depth (deep)

    5. Copy folder with images to default home directory, for example the folder name is em_images_tifs

        so now you have folder /home/<user>/em_images_tifs/
        
    6. Generate destination folder em_images and info file, so neuroglancer viewer can read precomputed images properly
        (assumes you have folder /home/<user>/precomputed and 
                 you have file /home/<user>/info_em_images.json)

        Open new terminal window and execute:
        
            generate-scales-info info_em_images.json precomputed/em_images/
    
                this command consists of calling "generate-scales-info" function with parameters 
                "json file path and name"  equal to "info_em_image.json"
                "destination folder where to put info file" equal to "precomputed/em_images/"
  
    7. Generate the 1:1 scale images in a precomputed format
        (using same terminal window as previous step)

        slices-to-precomputed --input-orientation RAS em_images_tifs/ precomputed/em_images/

            this command is calling "slices-to-precomputed" with parameters
            
            "--input-orientation" equal to "RAS", "RAS" gives the same orientation of images as in ImageJ 
            
                for more info take a look into file here:
                    https://github.com/HumanBrainProject/neuroglancer-scripts/blob/master/src/neuroglancer_scripts/scripts/slices_to_precomputed.py

            "folder with tif images" equal to "em_images_tifs/"
            
            "folder where to put precomputed images" equal to "precomputed/em_images"
          
    8. Generate other scales(scaled down images)    
        (using same terminal window as previous step)
        
        compute-scales --downscaling-method average precomputed/em_images/
        
            command is calling "compute-scales" with parameters
        
            "--downscaling-method" equal to "average", average is critical for images

            "folder with 1:1 scale precomputed images" equal to "precomputed/em_images/"
    
    9. Check if neuroglancer can load the data
        
        9.1 Open new terminal window and restart docker
        
            sudo docker stop $(sudo docker ps -a -q)

            sudo docker rm $(sudo docker ps -a -q)
         
            cd neuroglancer

            sudo docker run --name neuroglancer -v /home/<YOUR USERNAME>/precomputed:/precomputed:ro -v "$PWD"/dist/dev:/neuroglancer:ro -p 8085:80 neuroglancer

        9.2 Open web browser and go to viewer link

            http://localhost:8085/

        9.3 In a panel you have on the right, where listed different types of data source, in a input filed
            insert the link

            precomputed://http://localhost:8085/precomputed/em_images

        9.4 Click somewhere in a black area below the input text field,
            click yellow button in the bottom-right "Create image"

    10. For generating segmentation steps are the same, except 
    
        10.1 you need another json
        
        {
            "type": "segmentation",
            "data_type": "uint16",
            "num_channels": 1,
            "mesh": "mesh",
            "scales": [
                {
                    "size": [1542, 1502, 1186],
                    "resolution": [10, 10, 5],
                    "voxel_offset": [0, 0, 0]
                } 
            ]
        }           
        
        change the "data_type", "size" and "resolution" 

        uint8 is "8 bit, grey images"
        uint16 is "16 bit, grey images"

        be aware of differences between jsons for images and segmentation :
        "type", "data_type" and new property "mesh"

        10.2 "--downscaling-method" equal to "majority", majority is critical for segmentation

        10.3 Full set of commands needed to generate segmentation

            generate-scales-info info_em_segmentation.json precomputed/em_segmentation/

            slices-to-precomputed --input-orientation RAS em_segmentation_tifs/ precomputed/em_segmentation/

            compute-scales --downscaling-method majority precomputed/em_segmentation/
        
        10.4 Go to web frowser where you have neuroglancer with images already running and add new layer
             by clicking small + sign near to tab "em_images" and use the link

             precomputed://http://localhost:8085/precomputed/em_segmentation
        
--- Generating mesh ---

    *the easiest part

    1. download segmentation_to_mesh.py file
    
    2. copy this file to default folder, path would be /home/<user>/segmentation_to_mesh.py
    
    3. Open new terminal window and run
        
        python segmentation_to_mesh.py em_segmentation_tifs/ precomputed/em_semgentation/mesh/ 10 10 5

            here you opening file "segmentation_to_mesh.py" using "python" program and passing there parameters:
            "where to get *.tif files with segmentation" equal to "em_segmentation_tifs/"
            "where to put resulting mesh" equal to "precomputed/em_segmentation/mesh/"
            "voxel size X in nanometers" equal to "10" 
            "voxel size Y in nanometers" equal to "10" 
            "voxel size Z in nanometers" equal to "5" 
            
            - when you doubleclick on segmentation, in a 3D window mesh should appear

            - if orientation of mesh is not correspond to segmentation 
              take a look at line #31 in file segmentation_to_mesh.py

                mesh = np.transpose(mesh, (1, 0, 2)) #convert to [X Y Z]

                where 0 is axis X, 1 is axis Y, and 2 is axis Z

                you might need to change order of axes to get proper positioning of the mesh

                ...so far RAS precomputed worked well for me with np.transpose(1,0,2)

    4. Go to neuroglancer and oubleclick on any segment to see if it mesh displayed in 3d

    
Good job, you are done! :)

Now you have full development setup and there is very little left to add...


-- Serving data from gcloud bucket to neuroglancer-demo --

https://neuroglancer-demo.appspot.com/ expect two additional options for slices-to-precomputed and compute-scales 

    --flat
    --no-gzip 


cors_config.json

[
    {
      "origin": ["https://neuroglancer-demo.appspot.com/"],
      "method": ["GET","POST","OPTIONS"],
      "responseHeader": ["Access-Control-Allow-Origin"],
      "maxAgeSeconds": 3600
    }
]

commands to gcloud utils console:

    gcloud auth login

    gsutil cors set cors_config.json gs://seg_chunk_one_slice

                                          ^
                                    this is the bucket name

bucket permissions: 
    allUsers -> Storage Object Viewer 

-- Notes --
compute-scales "--flat" option generates folder structure that localhost neuroglancer cannot load

docker serves folder precomputed/ via http and you can access dataset when debugging neuroglancer-python

neuroglancer [version July 2020] loading precomputed data in nanometers[nm], while neuroglancer python 
mesh computation via neuroglancer.LocalVolume(...) happening in meters [m], before I have saved mesh
and checked vertex coordinates they mostly was 1e-6 ... 1e-8, while in a viewer units was about 0.1 ... 1.
